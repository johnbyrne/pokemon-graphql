import { StyleSheet, Dimensions } from 'react-native'

export default StyleSheet.create({
  regularText: {
    fontFamily: 'PokemonSolid',
    fontSize: 11,
    color: '#000000',
    textShadowColor: '#000',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 5,
    textAlign: 'center',
    letterSpacing: 1.3
  },
  hollowText: {
    fontFamily: 'PokemonHollow',
    fontSize: 18,
    color: '#000000',
  }
})