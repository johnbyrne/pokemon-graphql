import React from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import {
  View,
  TouchableHighlight,
  ListView,
  Modal,
  StyleSheet,
  Text,
  FlatList,
  TouchableOpacity,
  Image,
  Dimensions,
  AsyncStorage,
  ActivityIndicator
} from 'react-native'
import appStyles from '../appStyles'
import { Icon } from 'expo'
import Colors from '../constants/Colors'
import { Col, Row, Grid } from "react-native-easy-grid"
import { withNavigationFocus } from 'react-navigation'

const allPokemonQuery = gql`
  query {
    pokemons(first: 150) {
      name
      image
    }
  }`

class Home extends React.Component {
  static navigationOptions = {
    header: null,
  } 

  constructor(props) {
    super(props)

    this.state = {
      pokemons: [],
      favourites: []
    }

    this.onPressHeart = this.onPressHeart.bind(this)
  }

  async componentDidMount() {
    try {
      const data = await AsyncStorage.getItem('FAVOURITES')
      if (data !== null) {
        console.log(data)
        this.setState({
          favourites: JSON.parse(data)
        })
      }
     } catch (error) {
       alert('no favourites found')
     }
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.allPokemonQuery.loading && !nextProps.allPokemonQuery.error) {
      this.setState({
        pokemons: nextProps.allPokemonQuery.pokemons
      })
    }
  }

  onPressHeart(item) {
    let favourites = this.state.favourites
    let index = favourites.indexOf(item.name)
    if (index > -1) {
      favourites.splice(index, 1)
    } else {
      favourites.push(item.name)
    }
    AsyncStorage.setItem('FAVOURITES', JSON.stringify(favourites));
    this.setState({
      favourites
    }, this.forceUpdate())
  }

  renderHeart(item) {
    let heart = this.state.favourites.indexOf(item.name) > -1 ?
      'md-heart' : 'md-heart-outline'
    return (
      <TouchableOpacity
        style={styles.heartIcon}
        onPress={() => this.onPressHeart(item)}
      > 
        <Icon.Ionicons
          name={heart}
          size={20}
          style={{ marginBottom: -3 }}
          color={Colors.heart}
        />
      </TouchableOpacity>
    )
  }

  renderItem = ({ item }) => {
    return (
      <View style={styles.pokemon}>
        <View>
          <Image
            style={styles.image}
            source={{uri: item.image}}
          />        
        </View>
        <View>
          <Grid>
            <Row>
              <Col size={4}>
                <Text style={[appStyles.regularText, styles.listingText]}>
                  {item.name}
                </Text>
              </Col>
              <Col size={1}>
                {this.renderHeart(item)}
              </Col>
            </Row>
          </Grid>
        </View>
      </View>
    )
  }

  render () {
    if (this.props.allPokemonQuery.loading) {
      return (
        <View style={{position: 'relative', top: 300}}>
          <ActivityIndicator size="large" color="#ffffff" />
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <View style={styles.title}>
          <Text style={[appStyles.regularText, styles.titleText]}>Pokemons</Text>
        </View>
        <View style={styles.list}>
          <FlatList
            style={styles.flatlist}
            data={this.state.pokemons}
            showsVerticalScrollIndicator={false}
            renderItem={this.renderItem}
            initialNumToRender={8}
            numColumns={3}
            keyExtractor={item => item.name}
            extraData={this.state}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
    backgroundColor: '#4A7FC5',
    alignItems: 'center'
  },
  title: {
    position: 'absolute',
    top: 10
  },
  titleText: {
    fontSize: 24,
    color: '#F07529',
    paddingTop: 10
  },
  list: {
    marginTop: 30
  },
  listingText: {
    color: '#FFEC93',
  },
  pokemon: {
    marginTop: 15
  },
  heartIcon: {
    marginLeft: 0,
    position: 'relative',
    bottom: 2
  },
  image: {
    margin: 5,
    width: Dimensions.get('window').width/4,
    height: Dimensions.get('window').width/4,
    borderRadius: 15
  }
})

export default graphql(allPokemonQuery, {name: 'allPokemonQuery'})(withNavigationFocus(Home))