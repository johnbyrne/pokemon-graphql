import React from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import {
  View,
  TouchableHighlight,
  ListView,
  Modal,
  StyleSheet,
  Text,
  FlatList,
  TouchableOpacity,
  Image,
  Dimensions,
  AsyncStorage,
  ActivityIndicator
} from 'react-native'
import appStyles from '../appStyles'
import Colors from '../constants/Colors'
import { Col, Row, Grid } from "react-native-easy-grid"
import Icon from 'react-native-vector-icons/MaterialIcons'
import { withNavigationFocus } from 'react-navigation'

const allPokemonQuery = gql`
  query {
    pokemons(first: 30) {
      id
      number
      name
      image
      classification
      attacks {
        special {
          name
          type
          damage
        }
      }
    }
  }`

class Favourites extends React.Component {
  static navigationOptions = {
    header: null,
  } 

  constructor(props) {
    super(props)

    this.state = {
      pokemons: [],
      favourites: []
    }

    this.onPressDetails = this.onPressDetails.bind(this)
  }

  async componentDidMount() {
    try {
      const data = await AsyncStorage.getItem('FAVOURITES')
      if (data !== null) {
        console.log(data)
        this.setState({
          favourites: JSON.parse(data)
        })
      }
     } catch (error) {
       alert('no favourites found')
     }
  }

  async componentWillReceiveProps(nextProps) {
    if (!nextProps.allPokemonQuery.loading && !nextProps.allPokemonQuery.error) {
      const favouritePokemons = await AsyncStorage.getItem('FAVOURITES')
      let allPokemons = nextProps.allPokemonQuery.pokemons
      let pokemons = allPokemons.filter((pokemon) => {
        return favouritePokemons.indexOf(pokemon.name) > -1
      })
      this.setState({
        pokemons
      })
    }
  }

  onPressDetails(item) {
    this.props.navigation.navigate('Details', {
      pokemon: item.name,
    })
  }

  renderSpecialAttacks(attacks) {
    return attacks.special.map((attack, index) => {
      return (
        <Row key={index}>
          <Col style={{width: 30}}>
            <Icon style={styles.gameIcon} name="gamepad" size={10}/>
          </Col>
          <Col style={styles.attackName}>
            <Text style={[appStyles.regularText, styles.listingText]}>
              {attack.name}
            </Text>
          </Col>
        </Row>
      )
    })
  }

  renderItem = ({ item }) => {
    return (
      <View style={styles.pokemon}>
        <Grid>
          <Row>
            <Col style={{width: 100}}>
              <Image
                style={styles.image}
                source={{uri: item.image}}
              />
            </Col>
            <Col size={4}>
              <Row>
                <Col>
                  <Text style={[appStyles.regularText, styles.pokemonName]}>
                    {item.name}
                  </Text>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Text style={[appStyles.regularText, styles.pokemonClassification]}>
                    {item.classification}
                  </Text>
                </Col>
              </Row>
              <Row>
                <Col style={{flexDirection: 'row'}}>
                  {this.renderSpecialAttacks(item.attacks)}
                </Col>
              </Row>
            </Col>
          </Row>
        </Grid>
      </View>
    )
  }

  render () {
    if (this.props.allPokemonQuery.loading) {
      return (
        <View style={{position: 'relative', top: 300}}>
          <ActivityIndicator size="large" color="#ffffff" />
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <View style={styles.title}>
          <Text style={[appStyles.regularText, styles.titleText]}>Favourites</Text>
        </View>
        <View style={styles.list}>
          <FlatList
            style={styles.flatlist}
            data={this.state.pokemons}
            showsVerticalScrollIndicator={false}
            renderItem={this.renderItem}
            initialNumToRender={8}
            numColumns={1}
            keyExtractor={item => item.name}
            extraData={this.state}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
    backgroundColor: '#1D6E4C',
    alignItems: 'center'
  },
  title: {
    position: 'absolute',
    top: 10
  },
  titleText: {
    fontSize: 24,
    color: '#F07529',
    paddingTop: 10
  },
  list: {
    marginTop: 30,
    width: '90%'
  },
  pokemonName: {
    color: '#D2E0DC',
    fontSize: 17,
    textAlign: 'left'
  },
  pokemonClassification: {
    color: '#FCE1C0',
    fontSize: 12,
    position: 'relative',
    bottom: 3,
    textAlign: 'left'
  },
  listingText: {
    color: '#FFEC93',
    textAlign: 'left'
  },
  pokemon: {
    marginTop: 15,
  },
  gameIcon: {
    textShadowColor: '#000',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 5,
    textAlign: 'center',
    letterSpacing: 1.3,
    color: '#00E7F5'
  },
  heartIcon: {
    marginLeft: 0,
    position: 'relative',
    bottom: 2
  },
  image: {
    margin: 5,
    width: 80,
    height: 80,
    borderRadius: 15
  },
  attacksRow: {
    alignItems: 'flex-start'
  },
  attackName: {
    width: 150,
    position: 'relative',
    right: 5
  }
})

export default graphql(allPokemonQuery, {name: 'allPokemonQuery'})(withNavigationFocus(Favourites))