import React, { Component } from 'react'
import { View, AsyncStorage, Platform } from 'react-native';
import { AppLoading, Asset, Font } from 'expo'
import { createStackNavigator } from 'react-navigation'
import { createBottomTabNavigator } from 'react-navigation';
import Home from './Home'
import Favourites from './Favourites'
import { ApolloProvider } from 'react-apollo'
import { ApolloClient, HttpLink, InMemoryCache } from 'apollo-client-preset'
import fontCache from './data/fontCache'
import TabBarIcon from './components/TabBarIcon';

const httpLink = new HttpLink({ uri: `https://graphql-pokemon.now.sh/` })

const client = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache()
})

const HomeStack = createStackNavigator({
  Home,
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
};

const FavouritesStack = createStackNavigator({
  Favourites
});

FavouritesStack.navigationOptions = {
  tabBarLabel: 'Favourites',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={focused ? 'md-heart' : 'md-heart-outline'}
    />
  ),
};

const AppRoot = createBottomTabNavigator({
  HomeStack,
  FavouritesStack,
},{
  headerMode: 'none',
  tabBarOptions: {
    activeTintColor: '#ffffff',
    inactiveTintColor: '#C4C6D0',
    tabStyle: {
      backgroundColor: '#003156',
    },
    labelStyle: {
      fontSize: 14,
      fontFamily: 'PokemonSolid'
    }
  }
});

function cacheFonts(fonts) {
  return fonts.map(font => Font.loadAsync(font));
}

export default class App extends React.Component {
  state = {
    isReady: false,
  }

  async _loadAssetsAsync() {
    const fontAssets = cacheFonts(fontCache)
    await Promise.all([...fontAssets]);
  }

  render() {
    if (!this.state.isReady) {
      return (
        <View>
        <AppLoading
          startAsync={this._loadAssetsAsync}
          onFinish={() => {
            return this.setState({ isReady: true })
          }}
          onError={console.warn}
        />
        </View>
      )
    }

    return (
      <ApolloProvider client={client}>
        <AppRoot/>
      </ApolloProvider>
    )
  }
}
