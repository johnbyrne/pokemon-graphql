const tintColor = '#2f95dc';

export default {
  tintColor,
  tabIconDefault: '#C4C6D0',
  tabIconSelected: '#ffffff',
  tabBar: '#5E8051',
  heart: '#FF2D0B'
};
